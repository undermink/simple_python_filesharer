from flask import Flask,render_template,request,send_from_directory,redirect
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename
import os, random
import crypt

app = Flask(__name__)
auth = HTTPBasicAuth()

#UPLOAD_FOLDER = '/www/daten.sunnata.de/'
UPLOAD_FOLDER = '/tmp/linus/'
TRASH_FOLDER = '/tmp/trash_linus/'
USER_FILE = '/tmp/linus_user'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['TRASH_FOLDER'] = TRASH_FOLDER
app.config['USER_FILE'] = USER_FILE

list_of_files = {}
file_size = {}

# tbd
# ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

users = {
    "marc": generate_password_hash("marc"),
}

def salt():
    """Returns two random chars"""
    letters = 'abcdefghijklmnopqrstuvwxyz' \
              'ABCDEFGHIJKLMNOPQRSTUVWXYZ' \
              '0123456789/.'
    return random.choice(letters) + random.choice(letters)

@auth.verify_password
def verify_password(username, password):
    if username in users and \
            check_password_hash(users.get(username), password):
        return username

@app.route('/')
def index():
    list_of_files = {}
    file_size = {}
    for filename in os.listdir(app.config['UPLOAD_FOLDER']):
        list_of_files[filename] = "/"+filename
        file_size[filename] = os.path.getsize(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    return render_template('index.html', files=list_of_files, file_size=file_size)

@app.route('/admin')
@auth.login_required
def admin():
    return render_template('admin.html', username=auth.current_user())

@app.route('/upload', methods=['POST'])
@auth.login_required
def upload_file():
    if request.method == 'POST':
        files = request.files.getlist('files')
        for f in files:
            f.save(app.config['UPLOAD_FOLDER'] + secure_filename(f.filename))
        return render_template('confirm_upload.html')

@app.route('/admin/user')
@auth.login_required
def user_index():
    f = open(app.config['USER_FILE'], 'r')
    external_users = f.readlines()
    f.close()
    #print(external_users)
    return render_template('user.html', users=external_users)

@app.route('/admin/new_user', methods=['GET', 'POST'])
@auth.login_required
def new_user():
    if request.method == 'POST':
        user = request.form.get('user')
        password = crypt.crypt(request.form.get('password'), salt())
        f = open(app.config['USER_FILE'], 'a')
        f.write("%s:%s\n" % (user, password))
        f.close()
        return render_template('confirm_new_user.html', user=user,password=request.form.get('password'))

    return render_template('new_user.html')

@app.route('/admin/delete', methods=['GET', 'POST'])
@auth.login_required
def delete_files():
    if request.method == 'POST':
        for filename in os.listdir(app.config['UPLOAD_FOLDER']):
            if filename in request.form:
                os.rename(app.config['UPLOAD_FOLDER'] + filename, app.config['TRASH_FOLDER'] + filename)
        return redirect('/admin/delete')

    list_of_files = {}
    file_size = {}
    for filename in os.listdir(app.config['UPLOAD_FOLDER']):
        list_of_files[filename] = "/"+filename
        file_size[filename] = os.path.getsize(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    return render_template('delete_files.html', files=list_of_files)

@app.route('/admin/trash', methods=['GET', 'POST'])
@auth.login_required
def trash():
    if request.method == 'POST':
        for filename in os.listdir(app.config['TRASH_FOLDER']):
            if filename in request.form:
                os.rename(app.config['TRASH_FOLDER'] + filename, app.config['UPLOAD_FOLDER'] + filename)
        return redirect('/admin/trash')

    list_of_files = {}
    file_size = {}
    for filename in os.listdir(app.config['TRASH_FOLDER']):
        list_of_files[filename] = "/"+filename
        file_size[filename] = os.path.getsize(os.path.join(app.config['TRASH_FOLDER'], filename))
    return render_template('trash.html', files=list_of_files)

@app.route('/files/<path:path>')
def send_js(path):
    return send_from_directory(app.config['UPLOAD_FOLDER'], path)

if __name__ == '__main__':
    app.run()
