# simple python fileshare

Every time I needed to share a file with somebody, I usually wrote a quick index.html file and copied the files I wanted to share on some webserver. Then I would setup a simple basic auth htpasswd file with the users in it, I'd like to share my files with. So I came up with this simple python fileshare flask thing, which has a webinterface for all that. But let's say a really simple one :)

# Usage

## Configuration

There is no config file yet, but you can set up the folders at the beginning of the datenserver.py file.
There are some variables:

* `UPLOAD_FOLDER` – where the uploaded files are stored
* `TRASH_FOLDER` – where the deleted files are
* `USER_FILE` – the htpasswd-file

Then you probaply should set up a admin user. You can do this just a little bit down the same file. There is:

```
users = {
    "marc": generate_password_hash("marc"),
}
```

These users are admins.

## How to run it

You can run this like any flask app by exporting the FLASK_APP enviornment variable `export FLASK_APP=datenserver.py` and then run flask by typing `flask run`. Of course you'd need to have python and lfask already installed :)
If you do so, the app will open port 8000 or any other you define by adding `--port 1234` to your `flask run` command. See [the full flask documentation](https://flask.palletsprojects.com/en/1.1.x/) for details.
I usually proxy the app with a webserver like apache oder nginx. Then I let the app write the httpasswd file and I set up another basic auth from within the app for the admin access. Then I remove the basic auth from my apache or nginx file for the admin routes (eg. /admin ) - so that we don't have a double basic auth. That way I can use the webinterface to upload new files, delete them or add users...

# Author

undermink <undermink@chaostal.de>

# Licence

[WTFPLv2](http://www.wtfpl.net/about/)
